import 'package:flutter/material.dart';
import 'package:yaru/yaru.dart';
import 'package:yaru_widgets/yaru_widgets.dart';
import 'calculator.dart';

Future<void> main() async {
  await YaruWindowTitleBar.ensureInitialized();
  runApp(const goNona());
}

class goNona extends StatelessWidget {
  const goNona({super.key});
  @override
  Widget build(BuildContext context) {
    return YaruTheme(builder: (context, yaru, child) {
      return MaterialApp(
        title: 'গনণা',
        debugShowCheckedModeBanner: false,
        theme: yaru.theme,
        darkTheme: yaru.darkTheme,
        home: const gonoNa(),
      );
    });
  }
}
