import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:yaru_icons/yaru_icons.dart';
import 'package:yaru_widgets/yaru_widgets.dart';
//import 'package:handy_window/handy_window.dart';

class gonoNa extends StatefulWidget {
  const gonoNa({Key? key}) : super(key: key);

  @override
  State createState() => gononaPage();
}

class gononaPage extends State<gonoNa> {
  //String displayedNumber = '';
  TextEditingController t1 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const YaruWindowTitleBar(),
      body: Container(
        alignment: Alignment.bottomCenter,
        child: Column(
          children: [
            Expanded(
              flex: 3,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                alignment: Alignment.centerRight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      flex: 3,
                      child: SizedBox(
                        height: 40,
                        width: 100,
                        child: TextField(
                          controller: t1,
                          autofocus: true,
                          onSubmitted: (value) =>
                              t1.text = evaluateExpression(),
                          //onChanged: (value) {
                          //  setState(() {});
                          //},
                        ),
                      ),
                    ),
                    //Text(
                    //  displayedNumber,
                    //  style: const TextStyle(fontSize: 40),
                    //),
                    Padding(
                      padding: const EdgeInsets.all(2),
                      child: IconButton(
                          onPressed: () {
                            if (t1.text != "") {
                              setState(() {
                                t1.text =
                                    t1.text.substring(0, t1.text.length - 1);
                                //print(t1.text);
                              });
                            }
                            ;
                          },
                          iconSize: 20,
                          icon: const Icon(Icons.backspace_outlined)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 2),
                      child: YaruIconButton(
                        icon: const Icon(YaruIcons.edit_clear),
                        iconSize: 30,
                        onPressed: () {
                          //print(t1.text);
                          t1.text = "";
                          //print(t1.text);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            buildRow(['.', '0', '=', '+']),
            buildRow(['1', '2', '3', '-']),
            buildRow([
              '4',
              '5',
              '6',
              String.fromCharCode(int.parse("U+00d7".substring(2), radix: 16))
            ]),
            buildRow([
              '7',
              '8',
              '9',
              String.fromCharCode(int.parse("U+00F7".substring(2), radix: 16))
            ]),
          ],
        ),
      ),
    );
  }

  Widget buildRow(List<String> rowValues) {
    return Expanded(
      flex: 3,
      child: Row(
        children: rowValues.map((value) => numberpad(value)).toList(),
      ),
    );
  }

  Widget numberpad(String number) {
    return Expanded(
      flex: 3,
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: TextButton(
          onPressed: () {
            setState(() {
              if (number == '=') {
                t1.text = evaluateExpression();
              } else {
                t1.text += number;
              }
            });
          },
          style: ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(
              int.tryParse(number) == null || int.tryParse(number) == 0
                  ? (Theme.of(context).primaryColor)
                  : const Color.fromRGBO(60, 60, 60, 1),
            ),
            foregroundColor: const MaterialStatePropertyAll(Colors.white),
            overlayColor: const MaterialStatePropertyAll(Colors.white10),
            shape: const MaterialStatePropertyAll<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(18)),
                //side: BorderSide(color: Colors.white),
              ),
            ),
          ),
          child: SizedBox(
            height: double.maxFinite,
            child: Align(
              alignment: Alignment.center,
              child: Text(
                number,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 20,
                  //height: 5,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  String evaluateExpression() {
    // Add your own logic to evaluate the expression
    // For example, you can use the math_expressions package to parse and evaluate the expression
    // Here's a simple example that uses the built-in eval() function
    try {
      t1.text = t1.text.replaceAll('÷', '/').replaceAll('×', '*');
      //var Value = int.parse(displayedNumber);
      ContextModel cm = ContextModel();
      Parser parser = Parser();
      Expression exp = parser.parse(t1.text);
      var result = exp.evaluate(EvaluationType.REAL, cm);
      result == result.toInt() ? result = result.toInt() : result;
      return result.toString();
    } catch (e) {
      return 'Error';
    }
  }

  ShapeBorder shapeBorder = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(5.0),
    side: const BorderSide(
      color: Colors.black,
      width: 1.0,
    ),
  );
}
